import java.util.Scanner;

public class Hw07{
    
  static int whiteSpaceCounter= 0; // used for counting white space
    
  static int wordCounter = 1; // used for counting number of words
 
    static int phraseCounter = 0; // used for searching for phrases
    
    public static void main(String[] args) {
        
        // declare variables 
      Scanner myScanner = new Scanner (System.in);  
      boolean check = true; 
        String example = ""; 
        String menu = ""; 
        String phrase = ""; 
         
        
        System.out.println ("Enter a text example:");
        
       example = myScanner.nextLine();
        sampleText(example);
        
        // print's menu options
        System.out.println ("Menu");
        System.out.println ("c = Number of characters");
        System.out.println ("w = Word count");
        System.out.println ("f = Find text");
        System.out.println ("s = Shorten spaces");
        System.out.println ("r = replaces !'s");
        System.out.println ("q = Quit");
        System.out.println ("Choose option");
        
        // loop for menu options
        while (check) {
            
          menu = myScanner.nextLine();
            
       if (menu.equalsIgnoreCase("c") || menu.equalsIgnoreCase("w") || menu.equalsIgnoreCase("f") 
           || menu.equalsIgnoreCase("s") || menu.equalsIgnoreCase("r") || menu.equalsIgnoreCase("q") ) 
            { 
                if (menu.equalsIgnoreCase("f") || menu.equalsIgnoreCase("F")) {
                    System.out.println ("What phrase/word would you like to search for");
                    phrase = myScanner.nextLine();
                }
                
                printMenu (menu , example , phrase);
                
            }
            else {
                
              System.out.println ("Enter a valid choice.");
            }
        }
        
        System.exit(0);
    }
    //States what user entered
    public static void sampleText (String x) {
        
        System.out.println ("You entered this: " + x);
        
    }
    
    public static void printMenu (String y , String text , String phrase) {
        // Enters multiple methods
        // user input is y, text is original text, 
        // phrase is text user entered if input 'f'
        switch (y) {
            case "c":
                getNumOfNonWSCharacters(text);
                break;
            case "C":
                getNumOfNonWSCharacters(text);
                break;
            case "f":
                findText(text , phrase);
                break;
            case "F":
                findText(text , phrase);
                break;
            case "r":
                System.out.println (replaceExclamation(text));
                break;
            case "R":
                System.out.println (replaceExclamation(text));
                break;
            case "s":
                System.out.println (shortenSpace(text));
                break;
            case "S":
                System.out.println (shortenSpace(text));
                break;
            case "q":
                quit();
                break;
            case "Q":
                quit();
                break;
            case "w":
                getNumOfNonWSCharacters(text);
                break;
            case "W":
                getNumOfNonWSCharacters(text);
                break;
             
    }
        
        System.out.println ("Choose other Option");
        System.out.println ("Press (q) to quit");
    }
    
    public static void whiteSpaceCharacters (String text) {
        // runs through each char and counts nonwhitespaces
        for (int x = 0; x<text.length(); x++) {
            if (text.charAt(x) == ' ') {
                
            }
            else {
                whiteSpaceCounter++;
            }
        }
        System.out.println ("Number of non-whitespace characters: " + whiteSpaceCounter);
    }
    
    public static void getNumOfNonWSCharacters (String text) {
        // goes through each char and counts the number of words
        // up when it reaches a space
        for (int x = 0; x< text.length(); x++) {
            if (text.charAt(x) == ' ') {
                wordCounter++;
            }
            else {
                
            }
        }
        System.out.println ("Word count: " + wordCounter);
    }
     
    public static String replaceExclamation (String text) {
        // string method to replace exclamation points with period
        text = text.replaceAll ("!" , ".");
        return text;
    }
    
    public static void findText (String text , String phrase) {
        // removes the phrase from the text, finds difference of old length and new length,
        // then divides by the phrase length
        int phraseLength = phrase.length();
        int initialLength = text.length();
        text = text.replaceAll (phrase, "");
        int lengthNew = text.length();
        phraseCounter = (initialLength - lengthNew) / phraseLength;
        System.out.println ("'" + phrase + "' occurrences: " + phraseCounter);
    }
    
   
    
    public static String shortenSpace (String text) {
        text = text.replaceAll ("  " , " ");
        return text;
    }
    
    public static void quit() {
        // exits the program
        System.exit(0);
    }
    
    
}
