/// Will Gunn Lab03, 9/12
///This is a program that uses Scanner to get the  cost of the check,
///percent tip they pay, 
///and number of ways check will be split

import java.util.Scanner;

public class Check{
  //main method
  public static void main (String[] args) {
  Scanner myScanner = new Scanner ( System.in); // scanner command
    //Check cost
    System.out.print( "Enter original cost of check in from xx.xx: "); //Scanner input for check cost
    double checkCost = myScanner.nextDouble(); //variable for cost of check
    //Percent tip
    System.out.print( "Enter percentage tip to pay in xx): ");
    double tipPercent = myScanner.nextDouble(); //vairable for tip percent
    tipPercent /= 100; //converts to two decimals
    //number of people
    System.out.print( "Enter number of people who went to dinner:");
    int numPeople = myScanner.nextInt();
    //print output
    double totalCost;
    double costPerPerson;
    int dollars, dimes, pennies; //store dollars and cents
    totalCost = checkCost * (1+tipPercent);
    costPerPerson = totalCost / numPeople;
    //get whole amount dropping decimal fraction
    dollars = (int) costPerPerson;
    //gets dimes amount
    dimes=(int) (costPerPerson * 10) % 10;
    pennies=(int) (costPerPerson * 100) % 10;
    System.out.println ( "Each person in group owes $" +dollars+ '.' + dimes + pennies);
    
    
    
  } //end main method
  
}// end of class