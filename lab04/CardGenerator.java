/// lab 04, Will Gunn, 
//program will randomly generate a card from a deck of cards

//main method
public class CardGenerator{
  public static void main (String[] args) {
    
    //initialize cards
     String suit;
    int card = (int) (Math.random() * 52 + 1); //random card generator
   
    //suit assignment if statements
    if (card < 14) {
      suit = "Diamonds";
    }
    else if (card < 28) {
    suit = "Clubs";
    }
    else if (card < 42 ) {
     suit = "Hearts";
    }
    else {
     suit = "Spades";
    }
    
    //switch statements assigning card value
    switch (card) {
      case 1: //ace
      case 14:
      case 27:
      case 40:
      System.out.println("Ace of "+suit+""); 
      break;
        case 13: //king
      case 26:
      case 39:
      case 52:
      System.out.println("King of "+suit+"");
      break;
        case 12://queen
      case 25:
      case 38:
      case 51:
      System.out.println("Queen of "+suit+"");
      break;
        case 11: //jack
      case 24:
      case 37:
      case 50:
      System.out.println("Jack of "+suit+""); 
      break;
        case 15: //2
      case 28:
      case 41:
      System.out.println("2 of "+suit+""); 
      break;
      case 16:   //3
      case 29:
      case 42:
      System.out.println("3 of "+suit+""); 
      break; 
      case 17: //4
      case 30:
      case 43:
      System.out.println("4 of "+suit+""); 
      break;
       case 18: //5
      case 31:
      case 44:
      System.out.println("5 of "+suit+""); 
      break; 
        case 19: //6
      case 32:
      case 45:
      System.out.println("6 of "+suit+""); 
      break;
       case 20: //7
      case 33:
      case 46:
      System.out.println("7 of "+suit+""); 
      break;
      case 21: //8
      case 34:
      case 47:
      System.out.println("8 of "+suit+""); 
      break;
      case 22: //9
      case 35:
      case 48:
      System.out.println("9 of "+suit+""); 
      break;
      case 23: //10
      case 36:
      case 49:
      System.out.println("10 of "+suit+""); 
      break;  
      default:
      System.out.println(""+card+" of "+suit+"");
      break;
    }
    
  }
}