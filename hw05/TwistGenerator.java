////Will Gunn HW 05
  ///This program inputs a twist length from the user
 /// and then outputs twist with that  length 
////

import java.util.Scanner;

public class TwistGenerator{
  public static void main (String[] args){
    
    Scanner scnr = new Scanner (System.in);
    
    int length;
    String statement = "Enter a positive integer for length of twist";//initialize statement strings
    String errorMessage = "Wrong input, enter a positive integer.";
    
    System.out.print(statement); //prompt input
    while(!scnr.hasNextInt()||(length=scnr.nextInt())<= 0){ //check that it meets conditions
        System.out.println(errorMessage); //if conditions not met, output error message
        System.out.print(statement); // user input again
        scnr.nextLine(); // line 
    }
    
    scnr.nextLine(); // extra line
    
    String top = "\\ /"; //initialize 
    String middle = " x ";
    String bottom = "/ \\";
    int nmbrTwists = length/3; //calculate parts of twist 
    
    int twistModulus = length%3; //see how many partial twists ther are  
   
    switch(twistModulus){ //switch to see which type of output 
      
      case 0: //full twists 
        
         for(int x = 0;x<nmbrTwists;x++){ ///top loop
      System.out.print(top);
    } //top part 
    
        System.out.println(); 
     for(int x = 0;x<nmbrTwists;x++){ ///middle loop
      System.out.print(middle);
    }
    System.out.println();
    for(int x = 0;x<nmbrTwists;x++){ ///bottom loop
      System.out.print(bottom);
    }
        System.out.println(); //space 
        break; 
      case 1: //one part over
         for(int x = 0;x<nmbrTwists;x++){ ///top loop
      System.out.print(top);
    }
    
        System.out.print("\\"); //one part exra output 
    
        System.out.println();
   for(int x = 0;x<nmbrTwists;x++){ ///middle loop
      System.out.print(middle);
    }
    
        System.out.println(); //no middle extra 
     for(int x = 0;x<nmbrTwists;x++){ ///bottom loop
      System.out.print(bottom);
    }
        
        System.out.print("/"); //bottom extra output 
        System.out.println();
        break;
      case 2: //two parts over 
         for(int x = 0;x<nmbrTwists;x++){ ///top loop
      System.out.print(top);
    }
    
        System.out.print("\\"); //Extra output 
    System.out.println();
    for(int x = 0;x<nmbrTwists;x++){ ///middle loop
      System.out.print(middle);
    }
    
    System.out.print(middle); //Other middle output for two over 
    System.out.println();
      for(int x = 0;x<nmbrTwists;x++){ ///bottom loop
      System.out.print(bottom);
    }
    
        System.out.print("/"); //Extra output 
        System.out.println();
        break;    }    
  }
} 
