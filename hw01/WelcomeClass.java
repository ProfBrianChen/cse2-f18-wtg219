/////////////////
/* William Gunn
HW01
September 4th */
///
public class WelcomeClass
{
  
  public static void main (String args[])
  {
    System.out.println (" ----------- ");
    System.out.println (" | WELCOME | "); 
    System.out.println (" ----------- ");
    System.out.println ("  ^  ^  ^  ^  ^  ^ ");
    System.out.println (" / \\/ \\/ \\/ \\/ \\/ \\ ");
    System.out.println (" <-W--T--G--2--1--9-> ");
    System.out.println (" / \\/ \\/ \\/ \\/ \\/ \\ ");
    System.out.println (" v  v  v  v  v  v ");
    System.out.println ("Hello, my name is Will Gunn and I am a Senior Finance major and also on the Lehigh Lacrosse Team");                    
  }
                   
                       }
