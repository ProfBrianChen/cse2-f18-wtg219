/// Will Gunn Lab03, 9/17
///program that asks the user for doubles that represent the number of acres of land 
//affected by hurricane precipitation 
///and how many inches of rain were dropped on average.  
///It will also convert the quantity of rain into cubic miles.

import java.util.Scanner;

public class Convert{
  //main method
  public static void main (String[] args) {
    Scanner myScanner =  new Scanner(System.in);  //scanner
    //Variables
    double acresLand, rainFall; //acres of land affected, inches of rain dropped on average
   //input
    System.out.print("Number of acres affected: ");
    acresLand= myScanner.nextDouble();
    System.out.print("Inches of rainfall in affected area: ");
    rainFall= myScanner.nextDouble();
    //equations
    double totalRainfall= acresLand * rainFall;
    System.out.println("Total acre-inches of rain in affected area "+totalRainfall+"");
    //conversion
    double rainFallConversion = (totalRainfall * 27154.2857 * (9.08169e-13));// acre-inches to Gallons; 1 acre inch = 27154.2857 gallons; Gallons to Cubic Miles; 1 gallon = 9.08169e-13 cubic miles
    System.out.println("Rainfall in cubic miles "+rainFallConversion+"");

  
  }
}
    
    