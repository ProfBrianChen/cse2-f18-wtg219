/// Will Gunn Lab03, 9/17
//prompts the user for the dimensions of a pyramid 
//and returns the volume inside the pyramid.

import java.util.Scanner;

public class Pyramid{
  //main method
  public static void main (String[] args) {
    Scanner myScanner =  new Scanner(System.in);  //scanner
    
    //variables
    int sideSquare, pyramidHeight; //dimensions of pyramid
    //inputs
    System.out.print("A side of the square is: ");
      sideSquare = myScanner.nextInt(); //square side

    System.out.print("The pyramid height is: ");
    pyramidHeight=myScanner.nextInt(); //pyramid height 
    //pyramid volume equation
    int volumePyramid;
    volumePyramid = (((int) Math.pow(sideSquare,2) * (pyramidHeight))/3); //equation
    System.out.println("The volume of the pyramid is "+volumePyramid+""); //final output
    
  }
}
    //end method
    