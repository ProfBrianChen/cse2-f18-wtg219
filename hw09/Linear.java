//Will Gunn HW09 Linear
import java.util.Scanner;
import java.util.Arrays;

public class hw09Linear{
    public static void main (String[]args){
        
      System.out.println("Enter 15 ints");//  enter 15 integers
        Scanner scnr = new Scanner(System.in);
        
        int counter = 0;
        int[] array1 = new int[15];// array length 15
        
        while (counter < 15){
            
          if (scnr.hasNextInt()){// checks to see if input is integer
            
                array1[counter] = scnr.nextInt();
                
                if (array1[counter] < 0 || array1[counter] > 100){
                    
                  System.out.println("Integer inputed wasn't within  range 0-100");
                    System.exit(0);
                }
                
                else{
                    if (counter > 0){
                        if (array1[counter] < array1[counter - 1]){
                            
                          System.out.println(" intger inputed wasn't greater than past one ");
                            System.exit(0);
                        }
                    }
                counter++;
                }
            }
            
            else {
                System.out.println("No integer inputed");
                System.exit(0);
            }
        }
        
        System.out.println("Sorted: ");
        System.out.println(Arrays.toString(array1));
        System.out.print("Enter a grade to search for: ");
        
        if (scnr.hasNextInt()){
            
          int x = scnr.nextInt();
            
          BinarySearch(array1, x);
            Scramble(array1);
            
            System.out.println("Scrambled:");
            System.out.println(Arrays.toString(array1));
            System.out.print("Enter grade to search for: ");
            
            if (scnr.hasNextInt()){        // check to see if input is an integer
            
                LinearSearch(array1,x);
            }
            
            else{
               
              System.out.println("Input not an integer");
                System.exit(0);
            }
        }
        
        else
        {
            System.out.println("no grade to seach for");
            System.exit(0);
        }
    }
    
    
    public static void LinearSearch(int[] array1, int x){  // LinearSearch method
    
      for (int i = 0; i < array1.length; i++) {
        
            if (array1[i] == x){
                
              System.out.println(x + " was found in " + (i + 1) + " iterations");
                break;
            }
            
            else if (array1[i] != x && i == (array1.length - 1)){
                
              System.out.println(x + " was not found in " + (i + 1) + " iterations");
            }
        }
    }
   
    
    
    public static int[] Scramble(int[] array1)// Scramble method
    {
        for (int i = 0; i < array1.length; i++)// loops for length of the array 
        {
            int x = (int)(Math.random()*array1.length);
            int y = array1[i];
            while (x != i){
                
              array1[i] = array1[x];
                array1[x] = y;
                break;
            }
        }
        return array1;
    }
    
    
    public static void BinarySearch(int[] array2, int integer)
    {
        int high = array2.length - 1;
        int low = 0;
        int counter = 0;
        while (low <= high)
        {
            counter++;
            
            int mid = (high + low)/2;
            if (integer < array2[mid])
            {
                high = mid - 1;
            }
            else if (integer > array2[mid])
            {
                low = mid + 1;   
            }
            else if (integer == array2[mid])
            {
                System.out.println(integer + " was found in " + counter + " iterations");
                break;
            }
        }
        if (low > high)
        {
            System.out.println(integer + " wasn't found in " + counter + " iterations");
        }
    }
}