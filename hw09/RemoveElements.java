//Hw09 Will Gunn RemoveElements
import java.util.Scanner;

public class hw09RemoveElements{
  public static void main(String [] arg){
    
    Scanner scnr=new Scanner(System.in);
    
    int number[]=new int[10];
    int array1[];
    int array2[];
    int index,target;
   String answer="";
   
   do{
     
     System.out.print("Random input of 10 integers");
     number = randomInput();
     String out = "original array:";
     out += listArray(number);
     System.out.println(out);
     
      System.out.print("Enter index ");
      index = scnr.nextInt();
      if (index < 0 || index > number.length - 1){    //checks to see if index is between 0 and 9
      
        System.out.println(" Input not in the range 0-9");
        System.out.println(out);
      }
      
      else 
      {
      array1 = delete(number,index);
     
      String out1="output array is ";
      
      out1+=listArray(array1);  
      System.out.println(out1);
      }
   
      System.out.print("Enter target value ");
     target = scnr.nextInt();
     array2 = remove(number,target);
     String out2="output array is ";
     out2+=listArray(array2); 
     System.out.println(out2);
          
     System.out.print("to try again, enter 'y', anything else to quit-");
     answer=scnr.next();
   }
      while( answer.equals("y"));
  }
   
  
  public static String listArray(int num[]){
    String out="{";
    
    for(int j=0;j<num.length;j++){
     
      if(j>0){
        
        out+=", ";
     }
     
      out+=num[j];
    }
    
    out+="} ";
    return out;
  }
  
  
  
  public static int[] randomInput(){ 
  
    int number[]= new int[10];
    for (int i = 0; i < number.length; i++){
      int x = (int)(Math.random()*10);
      number[i] = x;
    }
    return number;
  }
  
  
  public static int[] delete(int[]array, int x){   // delete Method
    int [] array2 = new int[9];// new array of length 9 
    for (int i = 0; i < x; i++){
      
      array2[i] = array[i];
    }
    
    for (int j = x + 1; j < array.length; j++){ 
    
      array2[x] = array[j];
      x++;
    }
    return array2;
  }
  
  public static int[] remove(int[]array, int y){  // remove Method
  
    int counter = 0;
    for (int i = 0; i < array.length; i++){
    
      if (array[i] != y)
      {
        counter++;
      }
    }
    
    int array2[] = new int [counter];
    int counter2 = 0;
    for (int j = 0; j < array.length; j++){
    
      if (array[j] != y){
       
        array2[counter2] = array[j];
        counter2++;
      }
    }
    return array2;
  }
}