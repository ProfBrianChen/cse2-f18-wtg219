/// lab 04, Will Gunn, 
//program will ask user if they'd like randomly cast dice or if they'd like to choose dice
//and determine the outcome of the roll using if statements
//main method

// import scanner and rand gen
import java.util.Scanner;
import java.util.Random;

//main method
public class Crapsif{
  public static void main (String[] args) {
   
  Scanner myScanner = new Scanner ( System.in);
  Random randGen= new Random();
    
    //Initialize variables
    String choice = "blank";
    int dice1 = 0;
    int dice2 = 0;
    
        //ask user if they want random cast dice or if they want to state
    System.out.println("Do you want random cast die:");
    choice = myScanner.nextLine();
 
    // if statements for dice choice
    if (choice.equals ("Yes")) {       ///Yes
      dice1=randGen.nextInt (6)+1;
      dice2=randGen.nextInt (6)+1;
      
    }
    else if (choice.equals("No")) {    ///No
      System.out.println("Okay. Choose die 1 value:");     ///input 1
        dice1= myScanner.nextInt();   //die 1 value input
        
        System.out.println("Choose die 2 value:"); ////input 2
      dice2= myScanner.nextInt();
    }
    
    //die sum
    int dieTotal= dice2 + dice1;
    
    // Dice roll result slang terms
    if (dieTotal == 2) {
      System.out.println("Snake Eyes");     ///Snake Eyes
    }
    else if (dieTotal == 3) {
      System.out.println("Ace Deuce");     ///Ace Deuce
      
    }
     else if (dieTotal == 4 && dice1 == 2) {    ////Hard Four
      System.out.println("Hard Four");        
    }
     else if (dieTotal == 4 && dice1 != 2) { ////Easy Four
      System.out.println("Easy Four");
    }
    else if (dieTotal == 5 ) {
      System.out.println("Fever Five");     ////Fever Five
    }
    else if (dieTotal == 6 && dice1 != 3) {   ////Easy Six
      System.out.println("Easy Six");
    }
    else if (dieTotal == 6 && dice1 == 3) {    ////Hard Six
      System.out.println("Hard Six");
    }
    else if (dieTotal == 7) {
      System.out.println("Seven Out");     ////Seven Out
    }
    else if (dieTotal == 8 && dice1 != 4) {     ////Easy Eight
      System.out.println("Easy Eight");
    }
    else if (dieTotal == 8 && dice1 == 4) {    ////Hard Eight
      System.out.println("Hard Eight");
    }
    else if (dieTotal == 9) {
      System.out.println("Nine");    ////Nine
    }
    else if (dieTotal == 10 && dice1 != 5) {     ////Easy Ten
      System.out.println("Easy Ten");
    }
    else if (dieTotal == 10 && dice1 == 5) {     
      System.out.println("Hard Ten");        ////Hard Ten
    }
    else if (dieTotal == 11) {
      System.out.println("Yo-Leven");     ////Yoleven
    }
    else if (dieTotal == 12) {
      System.out.println("Boxcars");       ////Boxcars
    } 
  }
  
  
} ////end main method

