/* William Gunn, Skylar Schneider, Michael Weimann */
//
public class VenmoVariables{
  
public static void main(String args[]){
  /// defining variables
  double receiveAmt;
  // Amount received from transaction
  receiveAmt = 10;
  double sentAmt;
  //Amount sent from transaction
  sentAmt = 1;
  String cellNumber;
  //user id through phone number
  cellNumber = "123-456-7890";
  double currentBalance;
  // current balance on Venmo account
  currentBalance = 20;
  double updatedBalance = currentBalance + receiveAmt - sentAmt;
  // updated balance after transaction occurs
  double feeTransfer;
  // fee of transfering money immediately to bank account
   feeTransfer = 0.25;
  System.out.println(updatedBalance);
}
}