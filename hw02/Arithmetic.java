///////
/*   HW 02: Arithmetic Calculations
William Gunn, CSE 002, 9/11/2018 */
//
public class Arithmetic
{
  
  public static void main (String args[])
  {
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;
    
    //Number of sweatshirts
    int numShirts = 2;
    // cost per shirt
    double shirtPrice = 24.99;
    
    //Number of belts
    int numBelts = 1;
    // cost per belt
    double beltPrice = 33.99;
    
    //the tax rate
    double paSalesTax = 0.06;
    
    //Variables
    double totalCostBelts,totalCostPants,totalCostShirts;
    
    //total cost of items 
     totalCostPants = (numPants * pantsPrice);    //Pants cost
     totalCostShirts = numShirts * shirtPrice;  //Shirt cost
    totalCostBelts = numBelts * beltPrice;     //Belt cost
    
   System.out.println("Pants will cost $"+totalCostPants+"");
      System.out.println("Shirts will cost $"+totalCostShirts+"");
   System.out.println("Belts will cost $"+totalCostBelts+"");
    
    //Sales tax on each purchase
double salesTaxPants, salesTaxShirts, salesTaxBelts;  
    salesTaxPants = totalCostPants*paSalesTax;
    ///sales tax rounded to 2 decimals
    salesTaxPants = salesTaxPants*100; //rounded 
    int salesTaxPants1 = ((int)salesTaxPants);
    double salesTaxPants2;
    salesTaxPants2 = salesTaxPants1 / 100.00;
      
    salesTaxShirts = totalCostShirts*paSalesTax;
    salesTaxShirts = salesTaxShirts*100; //rounded 
    int salesTaxShirts1 = ((int)salesTaxShirts);
    double salesTaxShirts2;
    salesTaxShirts2 = salesTaxShirts1 / 100.00;
    
      salesTaxBelts = totalCostBelts*paSalesTax;
    salesTaxBelts = salesTaxBelts*100; //rounded 
    int salesTaxBelts1 = ((int)salesTaxBelts);
    double salesTaxBelts2;
    salesTaxBelts2 = salesTaxBelts1 / 100.00;
    
    System.out.println("Sales tax on pants will cost $"+ (salesTaxPants2) +"");
   System.out.println("Sales tax on shirts will cost $"+ (salesTaxShirts2) +"");
   System.out.println("Sales tax on belts will cost $"+ (salesTaxBelts2) +"");
    
    //Total cost of purchases before tax
    double totalCostPurchases;
    totalCostPurchases = totalCostBelts + totalCostShirts + totalCostPants;
    System.out.println ("The total cost of this purchase before tax is $"+(totalCostPurchases)+"");
    
    //Total Sales tax of purchases
    double totalSalesTax;
    totalSalesTax = salesTaxBelts + salesTaxShirts + salesTaxPants ;
    System.out.println ("Total sales tax of the transaction will be $"+(totalSalesTax)+"");
    
         //Total Cost of transaction with tax
    double totalCostTransaction;
    totalCostTransaction = totalSalesTax + totalCostPurchases;
    totalCostTransaction = totalCostTransaction*100; //rounded
    int totalCostTransaction1 = ((int)totalCostTransaction);
    double totalCostTransaction2;
    totalCostTransaction2 = totalCostTransaction1 / 100.00;
    System.out.println ("Total cost of transaction with tax will be $"+(totalCostTransaction2)+"");
 

 
  }
}