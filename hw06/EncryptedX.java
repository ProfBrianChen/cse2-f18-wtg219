///hw06, William Gunn, Encrypted X
//practice using nested loops

import java.util.Scanner;  //Scanner import

public class EncryptedX {
  public static void main (String args[]) {
    
  //declare variables
    int z=0;
    
   //ask for integer 1-100
    System.out.println("Please input an integer between 1-100:"); 
//Scanner declare
    Scanner scnr = new Scanner(System.in);

    //validate input
    
   while(!scnr.hasNextInt()||(z=scnr.nextInt())<1||z>100){
      
          System.out.println("Invalid number, enter a number 1-100"); ///error message
      System.out.println("Input a number 1-100:");
      
      scnr.nextLine();
    }
   scnr.nextLine(); //eat line
    
   //method for outputing secret message
    for(int x=0; x<=z; ++x) { //loop for rows
    
      for(int y=0; y<=z; ++y){ //loop for columns
      
        if((x==y)||(x+y)==z){
      
          System.out.print(" ");
      }
      else{
    System.out.print('*');
    }
    }
    System.out.println(); //new row
    }  
    
    }}