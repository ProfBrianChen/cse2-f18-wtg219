/////////////
//// lab02, William Gunn, 9/5/18, CSE 002
///
public class Cyclometer{
  ///Cyclometer lab 02 will record data of Cycling
  public static void main (String args[]){
  // input data
    int secsTrip1 = 480; // records seconds of first trip
    int secsTrip2 = 3220; // records seconds of second trip
    int countsTrip1 = 1561; // record of rotations of the first trip
    int countsTrip2 = 9037; // record of rotations of second trip
    
    //variables needed in doubles
    double wheelDiameter = 27.0, // size of wheel 
    PI = 3.14159, //
  	feetPerMile=5280,  // ft in mile
  	inchesPerFoot=12,   // inch in ft
  	secondsPerMinute=60;  // sec in min
	double distanceTrip1, distanceTrip2,totalDistance; //double of distance of trips
   System.out.println( "Trip 1 took " + (secsTrip1/secondsPerMinute)+ " minutes and had " + countsTrip1 + " counts."); //calculates time and rotations for trip 1 with values
   System.out.println( "Tip 2 took " +(secsTrip2/secondsPerMinute) + " minutes and had "+ countsTrip2+ "counts."); //calculates time and rotations for trip 2 with values
//
    //
    distanceTrip1=countsTrip1 * wheelDiameter * PI/inchesPerFoot / feetPerMile; // gives distance in inches
                          // for each count, a rotation of the wheel travels the diameter in inches times PI for trip1
    distanceTrip2=countsTrip2 * wheelDiameter * PI/ inchesPerFoot / feetPerMile;  //// gives distance in inches
                          // for each count, a rotation of the wheel travels the diameter in inches times PI for trip 2
    totalDistance=distanceTrip1+distanceTrip2;
    System.out.println ("Trip 1 was "+distanceTrip1+" miles");
    System.out.println ("Trip 2 was "+distanceTrip2+" miles");
    System.out.println ("Total distance was "+totalDistance+" miles");
    // final calculation of total distance
    
 
    
  }
  
}